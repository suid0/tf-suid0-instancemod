variable "compartment_id" {
  description = "Tenancy"
  type        = string
}
variable "subnet-sp-priv" {
  description = "Subnet Rede Privada"
  type        = string
}
variable "shape" {
  description = "Shape da máquina"
  type        = string
}
variable "MEM" {
  description = "Memória RAM"
  type        = number
}
variable "CPU" {
  description = "Número de CPUs"
  type        = number
}
variable "AvDomain" {
  description = "Avaiability Domain"
  type        = string
}
variable "ImageID" {
  description = "ID da Imagem"
  type = string
}
variable "pubkB" {
  description = "PlanoB para a pubkey"
  type = string
}
variable "ip_publico" {
  description = "Necessita de IP Publico?"
  type = bool
}
variable "private_dns" {
  description = "Será criado por padrão uma entrada de dns privado na cloud?"
  type = bool
}
variable "qtdemaq" {
  description = "Quantidade de maquinas para subir"
  type = number
}
variable "projeto" {
  description = "Nome do projeto para ser usado na maquina"
  type = string
}
