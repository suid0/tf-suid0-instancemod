# tf-oci-suid0mod-instance



## Purpose

The purpose of this module is to make it easier (almost without any coding) to create instances on Oracle Cloud - OCI

## License

This piece of code is under GPL, please make sure you help the community!

## How to use

You dont't need to download this repo to be able to use it. All you have to do is point this module in your .tf file

### Code you still need to create

If you're going to customize your backend to store tfstate files, you still need to code this.
Make sure you add a provider with the configurations you wan't to use.

### Required fields

```
module "instance" {
    source = "git::https://gitlab.com/svictorino/tf-oci-suid0mod-instance.git"
    compartment_id = "Compartment OCID"
    subnet-sp-priv = "Subnet OCID"
    shape          = "VM Shape"
    MEM            = "4" # Memory
    CPU            = "1" # No of vCPUs
    AvDomain       = "RepU:SA-SAOPAULO-1-AD-1" #Availabilty Domain
    ImageID        = "Image OCID"
    pubkB          = "Place your ssh pubkey here"
    ip_publico     = false  ## Do you wan't a plublic IP?
    private_dns    = false  ## Are you going to use OCI's private DNS?
    qtdemaq        = 10  ## How many VMs do you wan't to create?
    projeto        = "Name to use in machine's names" 
}
```

## Suggestions

Send-me an e-mail at: s@svic.me

